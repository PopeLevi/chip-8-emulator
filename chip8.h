#ifndef CHIP8_H
#define CHIP8_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

#include "global.h"
#include "display.h"
#include "timers.h"

typedef union
{
    byte val;

    struct
    {
	byte b0:1; byte b1:1; byte b2:1; byte b3:1;
	byte b4:1; byte b5:1; byte b6:1; byte b7:1;
    };

} mem8;

void cpu_init(uint32_t fcolour, uint32_t bcolour, int speed);
void cpu_destroy();

bool cpu_cycle();
void cpu_reset();

void load_rom(char* fname);

#endif
