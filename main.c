#include <stdio.h>
#include <stdlib.h>

#include "chip8.h"
#include "display.h"
#include "gui.h"

int main(int argc, char* argv[])
{
    bool quit = false;
    char* filename = NULL;
    uint32_t fg, bg;

    while (!quit)
    {
	filename = gui_init(&argc, &argv, &quit, filename);
	if (quit) exit(0);

	gui_getcolours(&fg, &bg);
	
	cpu_init(fg, bg, gui_getExecSpeed());
	load_rom(filename);
    
	while (cpu_cycle()) { }
	cpu_destroy();
    }

    gui_quit();
    return 0;
}
