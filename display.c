#include "global.h"
#include "display.h"
#include "timers.h"

uint32_t WINDOW_BACKCOLOUR = 0;
uint32_t WINDOW_FORECOLOUR = 0xFFFFFF;
const int    WINDOW_WIDTH      = 1024;
const int    WINDOW_HEIGHT     = 512;
const byte   PIXEL_SIZE        = 16;

const char* pauseMsgFile = "../res/pause.png";
const char* romEndMsgFile = "../res/rom_end.png";

SDL_Window* outputWindow;
SDL_Surface* windowSurface;
SDL_Surface* pauseSurface;
SDL_Surface* romEndSurface;
byte display_matrix[64][32];
bool keys[16];

bool paused = false;

const byte keymap[16] =
{
    SDL_SCANCODE_X, SDL_SCANCODE_1, SDL_SCANCODE_2, SDL_SCANCODE_3,
    SDL_SCANCODE_Q, SDL_SCANCODE_W, SDL_SCANCODE_E, SDL_SCANCODE_A,
    SDL_SCANCODE_S, SDL_SCANCODE_D, SDL_SCANCODE_Z, SDL_SCANCODE_C,
    SDL_SCANCODE_4, SDL_SCANCODE_R, SDL_SCANCODE_F, SDL_SCANCODE_V
};

void display_init()
{
    display_clear();

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
	printf("Unable to initialize SDL\n");
	exit(1);
    }

    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
    {
	printf("Unable to initialize SDL_image\n");
	exit(1);
    }

    outputWindow = SDL_CreateWindow("CHIP-8 Emulator", SDL_WINDOWPOS_UNDEFINED,
				  SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,
				  WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    
    if (outputWindow == NULL)
    {
	printf("Unable to initialize main window\n");
	exit(1);
    } else {
	windowSurface = SDL_GetWindowSurface(outputWindow);

	if (windowSurface == NULL)
	{
	    printf("Unable to grab window surface\n");
	    exit(1);
	} else {
	    SDL_ShowWindow(outputWindow);
	    display_draw();
	}
    }

    pauseSurface = IMG_Load(pauseMsgFile);
    if (pauseSurface == NULL)
    {
	printf("Unable to load %s: %s\n", pauseMsgFile, IMG_GetError());
	exit(1);
    }

    romEndSurface = IMG_Load(romEndMsgFile);
    if (romEndSurface == NULL)
    {
	printf("Unable to load %s: %s\n", romEndMsgFile, IMG_GetError());
	exit(1);
    }
}

void display_destroy()
{
    SDL_FreeSurface(windowSurface);
    SDL_DestroyWindow(outputWindow);
    SDL_Quit();
}

void display_pause(bool* exitCond, bool rom_end)
{
    bool quit = false;
    paused = true;
    SDL_Event event;

    SDL_Surface* overlay;
    SDL_Surface* messageSurface = rom_end ? romEndSurface : pauseSurface;
    SDL_Rect msgRect = { .x = (WINDOW_WIDTH / 2) - 250, .y = (WINDOW_HEIGHT / 2) - 100, .w = 500, .h = 200 };

    // Display a pause message overlay
    overlay = SDL_CreateRGBSurface(0, WINDOW_WIDTH, WINDOW_HEIGHT, 24, 0, 0, 0, 255);
    SDL_SetSurfaceAlphaMod(overlay, 220);
    SDL_SetSurfaceColorMod(messageSurface, (WINDOW_FORECOLOUR & 0xFF0000) >> 16, (WINDOW_FORECOLOUR & 0x00FF00) >> 8, (WINDOW_FORECOLOUR & 0x0000FF));
    SDL_BlitSurface(overlay, NULL, windowSurface, NULL);
    SDL_BlitScaled(messageSurface, NULL, windowSurface, &msgRect);
    SDL_UpdateWindowSurface(outputWindow);

    pause_sound();
    
    // Start a secondary message loop
    while (!quit)
    {
	while (SDL_PollEvent(&event))
	{
	    switch (event.type)
	    {
		case SDL_QUIT: *exitCond = true; quit = true; break;
		case SDL_KEYDOWN:
		    switch (event.key.keysym.sym)
		    {
			case SDLK_ESCAPE: *exitCond = rom_end; quit = true; break;
		    }

		    break;

		case SDL_WINDOWEVENT:
		    switch (event.window.event)
		    {
			case SDL_WINDOWEVENT_EXPOSED: SDL_UpdateWindowSurface(outputWindow); break; // Redraw the window when other windows are dragged across, otherwise visual artifacts will occur
		    }
		
		    break;
	    }
	}

	SDL_Delay(1);
    }

    paused = false;
    resume_sound();
    display_draw();
}

bool display_update(bool rom_end)
{
    SDL_Event event;
    bool ret = false;

    while (SDL_PollEvent(&event))
	switch (event.type)
	{
	    case SDL_QUIT: ret = true; break;

	    case SDL_WINDOWEVENT:
		switch (event.window.event)
		{
		    case SDL_WINDOWEVENT_EXPOSED: SDL_UpdateWindowSurface(outputWindow); break; // Redraw the window when other windows are dragged across, otherwise visual artifacts will occur
		}
		
		break;

	    case SDL_KEYDOWN:
	    {
		switch (event.key.keysym.sym)
		{
		    case SDLK_ESCAPE: display_pause(&ret, rom_end); break;
		}
	    }
	}

    return ret;
}

void display_draw()
{
    SDL_FillRect(windowSurface, NULL, WINDOW_BACKCOLOUR);
    
    for (byte y = 0; y < 32; y++)
    	for (byte x = 0; x < 64; x++)
    	{
    	    if (!display_matrix[x][y]) continue;
    	    SDL_Rect pixel = { .x = x * PIXEL_SIZE, .y = y * PIXEL_SIZE, .w = PIXEL_SIZE, .h = PIXEL_SIZE };
    	    SDL_FillRect(windowSurface, &pixel, WINDOW_FORECOLOUR);
    	}

    SDL_UpdateWindowSurface(outputWindow);
}

void display_kmatrix(bool* in)
{
    const byte* kstate = SDL_GetKeyboardState(NULL);
    
    for (byte b = 0; b < 16; b++)
	in[b] = kstate[keymap[b]];
}

void display_setcolours(uint32_t fg, uint32_t bg)
{
    WINDOW_FORECOLOUR = fg;
    WINDOW_BACKCOLOUR = bg;
}

byte display_setpixel(byte x, byte y)
{
    byte ret = display_matrix[x % 64][y % 32] ? 1 : 0;
    display_matrix[x % 64][y% 32] ^= 1;
    return ret;
}

void display_clear()
{
    memset(display_matrix, 0, sizeof(display_matrix));
}
