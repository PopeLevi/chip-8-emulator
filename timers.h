#ifndef TIMERS_H
#define TIMERS_H

#include "global.h"

void run_delay_timer(byte* data);
void init_sound();
void run_sound_timer(byte* data);
void pause_sound();
void resume_sound();
void quit_sound();

#endif
