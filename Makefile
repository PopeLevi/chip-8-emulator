CC=gcc
LIB = -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer

all: clean main.o chip8.o display.o gui.o timers.o
	$(CC) ./obj/main.o ./obj/chip8.o ./obj/display.o ./obj/gui.o ./obj/timers.o -o ./bin/chip8 $(LIB) `pkg-config --libs gtk+-3.0`

clean:
	if [ ! -d "./bin" ]; then mkdir ./bin; fi
	if [ ! -d "./obj" ]; then mkdir ./obj; fi

	if [ -f "./bin/chip8" ]; then rm -f ./bin/chip8; fi

main.o: main.c
	if [ -f "./obj/main.o" ]; then rm ./obj/main.o; fi
	$(CC) -g -c main.c -o ./obj/main.o -std=c11 `pkg-config --cflags gtk+-3.0`

chip8.o: chip8.c
	if [ -f "./obj/chip8.o" ]; then rm ./obj/chip8.o; fi
	$(CC) -g -c chip8.c -o ./obj/chip8.o -std=c11 `pkg-config --cflags gtk+-3.0`

display.o: display.c
	if [ -f "./obj/display.o" ]; then rm ./obj/display.o; fi
	$(CC) -g -c display.c -o ./obj/display.o -std=c11 `pkg-config --cflags gtk+-3.0`

gui.o: gui.c
	if [ -f "./obj/gui.o" ]; then rm ./obj/gui.o; fi
	$(CC) -g -c gui.c -o ./obj/gui.o -std=c11 `pkg-config --cflags gtk+-3.0`

timers.o: timers.c
	if [ -f "./obj/timers.o" ]; then rm ./obj/timers.o; fi
	$(CC) -g -c timers.c -o ./obj/timers.o -std=c11 `pkg-config --cflags gtk+-3.0`
