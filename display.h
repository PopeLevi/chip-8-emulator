#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "global.h"

bool keys[16];

void display_init();
void display_destroy();

bool display_update(bool rom_end);
void display_draw();

void display_kmatrix(bool in[]);

void display_setcolours(uint32_t bg, uint32_t fg);

byte display_setpixel(byte x, byte y);
void display_clear();

#endif
