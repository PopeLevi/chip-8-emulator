#include <SDL2/SDL.h>
#include "chip8.h"

mem8* memory;   // CHIP-8 memory
word stack[16]; // CHIP-8 stack
word PC;        // Program Counter
word I;         // Memory index pointer
byte _SP;        // Stack pointer
byte v[16];     // General purpose registers
byte ST, DT;    // Sound/Delay timers
byte draw_flag = 0;
bool rom_end = false;

/* char* rom = NULL; */
bool inputMatrix[16];

int execSpeed = 2;
long ticks[2];
long cycle_time;

// The sixteen hexadecimal characters available from the VM memory
const byte chars[] =
{
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
};

// The default program for if no ROM is selected when the emulator starts. Prints "No ROM"
byte no_rom[] =
{
    0x00, 0xE0, 0x60, 0x01, 0x61, 0x01,
    0xA2, 0x26, 0xD0, 0x15, 0x70, 0x05,
    0xA2, 0x2B, 0xD0, 0x15, 0x70, 0x0A,
    0xA2, 0x30, 0xD0, 0x15, 0x70, 0x05,
    0xA2, 0x2B, 0xD0, 0x15, 0x70, 0x05,
    0xA2, 0x35, 0xD0, 0x15, 0x12, 0x22,
    0x00, 0x00, 0x90, 0xD0, 0xB0, 0x90,
    0x90, 0x60, 0x90, 0x90, 0x90, 0x60,
    0xE0, 0xA0, 0xF0, 0x90, 0x90, 0x90,
    0xF0, 0xD0, 0x90, 0x90
};

void cpu_init(uint32_t fcolour, uint32_t bcolour, int speed)
{
    cpu_reset();
    srand(time(NULL));

    display_setcolours(fcolour, bcolour);
    display_init();

    execSpeed = speed;
    memset(ticks, 0, sizeof(ticks));
}

void cpu_destroy()
{
    free(memory);
    display_destroy();
}

void cpu_reset()
{
    memory = NULL;
    memory = calloc(0xFFF, 0x1); // Allocate 4096 bytes of memory
    memset(v, 0, sizeof(v)); // Set all elements of v to 0
    
    memcpy(memory, chars, 80); // Copy the characters into the first 80 bytes of memory
    
    PC = 0x200; // Start at address 512 as the first 512 bytes of RAM are reserved for the VM
    _SP = 0;
    ST = 0;
    DT = 0;
    I = 0;
    
    draw_flag = 0;
    rom_end = false;

    quit_sound();
    init_sound();
}

bool cpu_cycle()
{
    cycle_time = SDL_GetTicks();
    
    word opcode = (memory[PC].val << 8) | memory[PC + 1].val; // Concatenate the bytes to get the full opcode (2 bytes)
    word x = (opcode & 0x0F00) >> 8, y = (opcode & 0x00F0) >> 4;

    display_kmatrix(inputMatrix); // Receive input from the keyboard

    switch (opcode & 0xF000)
    {
	case 0x0000:
	    switch (opcode)
	    {
		case 0x00E0: display_clear(); break;
		case 0x00EE: if (_SP) PC = stack[--_SP]; break; // Return from subroutine
	    }

	    break;

	case 0x1000: if (PC == (opcode & 0x0FFF)) rom_end = true; PC = (opcode & 0x0FFF) - 2; break; // Jump to location in memory
	case 0x2000: stack[_SP++] = PC; PC = (opcode & 0x0FFF) - 2; break; // Push the program counter onto the stack and jump to NNN
	case 0x3000: PC += v[x] == (opcode & 0x00FF) ? 2 : 0; break; // Skip next instruction if VX == Y
	case 0x4000: PC += v[x] != (opcode & 0x00FF) ? 2 : 0; break; // Skip next instruction if VX != Y
	case 0x5000: PC += v[x] == v[y] ? 2 : 0; break; // Skip next instruction if VX == VY
	case 0x6000: v[x] = opcode & 0x00FF; break; // Set VX to NN
	case 0x7000: v[x] += opcode & 0x00FF; break; // Add NN to VX
	case 0x8000:
	    switch(opcode & 0x000F)
	    {
		case 0x0: v[x] = v[y]; break;
		case 0x1: v[x] |= v[y]; break;
		case 0x2: v[x] &= v[y]; break;
		case 0x3: v[x] ^= v[y]; break;
		case 0x4: v[0xF] = (v[x] + v[y] < v[x]) ? 1 : 0; v[x] += v[y]; break; // Add VX and VY, store the result in VX. If the result is larger than 255, VF to 1
		case 0x5: v[0xF] = (v[x] > v[y]) ? 1 : 0; v[x] -= v[y]; break; // Subtract VY from VX. Set VF to 1 if a borrow occurs
		case 0x6: v[0xF] = v[x] & 0x1; v[x] >>= 1; break; // Set VF to the LSB of VX and shift VX right
		case 0x7: v[0xF] = (v[y] > v[x]) ? 1 : 0; v[x] = v[y] - v[x]; break; // Set VX to VY - VX
		case 0xE: v[0xF] = ((v[x] & 0x80) >> 7); v[x] <<= 1; break; // Set VF to the MSB of VX and perform a left shift on VX
	    }

	    break;

	case 0x9000: PC += v[x] != v[y] ? 2 : 0; break; // Skip next instruction if VX != VY
	case 0xA000: I = opcode & 0x0FFF; break; // Set I to NNN
	case 0xB000: PC = (opcode & 0x0FFF) + v[0] - 2; break; // Set the program counter to NNN + V0
	case 0xC000: v[x] = (rand() & (opcode & 0x00FF)); break; // Set VX to a random number & NN
	case 0xD000: // Draw the sprite at I
	{
	    v[0xF] = 0;
	    byte h = opcode & 0x000F;

	    for (byte sY = 0; sY < h; sY++)
	    {
		byte line = memory[I + sY].val;

		for (byte sX = 0; sX < 8; sX++)
		    if (line & (0x80 >> sX))
			v[0xF] = display_setpixel(v[x] + sX, v[y] + sY); // If any pixels are overwritten, set VF to 1
	    }

	    draw_flag = 1;
	    break;
	}
	
	case 0xE000:
	    switch (opcode & 0x00FF)
	    {
		case 0x9E: PC += inputMatrix[v[x] % 16] ? 2 : 0; break; // Skip the next instruction if the key in VX is pressed
		case 0xA1: PC += inputMatrix[v[x] % 16] ? 0 : 2; break; // Skip the next instruction if the key in VX is NOT pressed
	    }

	    break;

	case 0xF000:
	    switch (opcode & 0x00FF)
	    {
		case 0x07: v[x] = DT; break;
		case 0x0A: // Wait for a keypress and store the result in VX
		{
		    for (byte b = 0; b < 16; b++)
		    {
			if (inputMatrix[b])
			{
			    v[x] = b;
			    goto end; // GOTO IS FINE USED IN MODERATION. THIS SAVES A BYTE OF MEMORY
			}
		    }

		    PC -= 2;
		    end:
		    break;
		}

		case 0x15: DT = v[x]; break;
		case 0x18: ST = v[x]; break;
		case 0x1E: I += v[x]; break;
		case 0x29: I = v[x] * 5; break;
		case 0x33: memory[I].val = (v[x] / 100) % 10; // Set memory[I], memory[I + 1], memory[I + 2] to the binary encoded decimal values of VX
		           memory[I + 1].val = (v[x] / 10) % 10;
			   memory[I + 2].val = v[x] % 10;
			   break;

		case 0x55: // Store V0 to VX in memory, starting at I
		    memcpy(memory + I, v, x + 1);
		    break;
		    
		case 0x65: // Read memory into registers V0 to VX, starting at I
		    memcpy(v, memory + I, x + 1);
		    break;
	    }

	    break;

	default: printf("*** UNKNOWN OPCODE ***"); return false;
    }

    PC += 2; // Move to the next instruction

    if (draw_flag)
    {
	display_draw();
	draw_flag = 0;
    }

    if (ticks[0] - ticks[1] >= (1000 / (120 / execSpeed))) // Count down at 60Hz (Relative to execution speed)
    {
	run_sound_timer(&ST);
	run_delay_timer(&DT);

	ticks[1] = ticks[0];
    }

    ticks[0] = SDL_GetTicks();

    SDL_Delay(execSpeed);
    return !display_update(rom_end);
}

void load_rom(char* fname)
{
    if (fname == NULL)
    {
	printf("No ROM detected. Loading default ROM\n");
	memcpy(memory + 0x200, no_rom, sizeof(no_rom)); // Load the default ROM into memory
	
	return;
    }

    FILE* file;
    char* buffer = NULL;
    long flength;

    file = fopen(fname, "rb");

    if (file)
    {
	fseek(file, 0, SEEK_END);
	flength = ftell(file);
	fseek(file, 0, SEEK_SET);

	buffer = malloc(flength);

	if (buffer)
	    fread(buffer, 1, flength, file);
	else
	    perror("Unable to allocate memory for file buffer");

	fclose(file);

	if (buffer)
	{
	    // Load the program into RAM
	    memcpy(memory + 0x200, buffer, flength);
	    free(buffer);
	}

    } else {
	perror("Cannot open ");
	perror(fname);
    }
}
