#include <stdint.h>
#include <stdbool.h>

typedef uint8_t byte;  // 1 byte
typedef uint16_t word; // 2 bytes
