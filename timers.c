#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL_mixer.h>

#include "timers.h"

Mix_Chunk* beepSound = NULL;
const char* beepFile = "../res/beep.wav";

void run_delay_timer(byte* data)
{
    (*data) -= *data ? 1 : 0;
}

void init_sound()
{
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
    {
	printf("Unable to initialize SDL2_mixer: %s", Mix_GetError());
	exit(1);
    }

    if (beepSound == NULL)
	if ((beepSound = Mix_LoadWAV(beepFile)) == NULL)
	{
	    printf("Cannot load beep.wav: %s", Mix_GetError());
	    exit(1);
	}
}

void run_sound_timer(byte* data)
{
    if (*data)
    {
	(*data)--;
	if (!Mix_Playing(0)) Mix_PlayChannel(0, beepSound, 0);
	else Mix_Resume(0);
	
    } else {
	if (Mix_Playing(0) && !Mix_Paused(0)) Mix_Pause(0);
    }
}

void pause_sound()
{
    Mix_Pause(0);
}

void resume_sound()
{
    Mix_Resume(0);
}

void quit_sound()
{
    Mix_CloseAudio();
}
