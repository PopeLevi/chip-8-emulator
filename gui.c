#include <string.h>
#include "gui.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#ifdef _WIN32
    #define DIR_SEPERATOR '\\'
#else
    #define DIR_SEPERATOR '/'
#endif

GtkWidget* mainWindow;
GtkWidget* mainVbox;

GtkWidget* runButton;
GtkWidget* loadButton;
GtkWidget* quitButton;
GtkWidget* statusbar;
GtkWidget* openFileDialog;

GtkWidget* optionsBox;

GtkWidget* delayExpander;
GtkWidget* delayFixed;
GtkWidget* delaySpinner;

GtkWidget* fcolourExpander;
GtkWidget* fcolourFixed;
GtkWidget* fcolourButton;
GdkRGBA fcolour;

GtkWidget* bcolourExpander;
GtkWidget* bcolourFixed;
GtkWidget* bcolourButton;
GdkRGBA bcolour;

GtkWidget* notebook;
GtkWidget* notebookLabels[2];

bool* quitCondition;
bool has_colour_init = false;
char* filename;
char displayName[256];
char statusbarText[256];
gint statusbarContext;

int executionSpeed = -1;

// Callback functions
static bool mainWindow_quit(GtkWidget* widget, gpointer data);
static bool runButton_click(GtkWidget* widget, gpointer data);
static bool loadButton_click(GtkWidget* widget, gpointer data);
static bool backButton_click(GtkWidget* widget, gpointer data);

char* gui_init(int* argc, char*** argv, bool* quitCond, char* fname)
{
    quitCondition = quitCond;
    filename = fname;

    gtk_init(argc, argv);

    // Init main window
    mainWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(mainWindow), "CHIP-8 Emulator");
    gtk_widget_set_size_request(mainWindow, 200, 300);
    gtk_window_set_resizable(GTK_WINDOW(mainWindow), false);
    gtk_window_set_position(GTK_WINDOW(mainWindow), GTK_WIN_POS_CENTER);
    g_signal_connect(mainWindow, "delete_event", G_CALLBACK(mainWindow_quit), NULL);

    // Init controls
    runButton = gtk_button_new_with_label("Run");
    loadButton = gtk_button_new_with_label("Load ROM");
    quitButton = gtk_button_new_with_label("Quit");
    gtk_widget_set_size_request(quitButton, 200, 10);

    statusbar = gtk_statusbar_new();
    statusbarContext = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "message");
    if (statusbarText) gtk_statusbar_push(GTK_STATUSBAR(statusbar), statusbarContext, statusbarText);

    openFileDialog = gtk_file_chooser_dialog_new("Select ROM file...", GTK_WINDOW(mainWindow),
						 GTK_FILE_CHOOSER_ACTION_OPEN,
						 "Cancel", GTK_RESPONSE_CANCEL,
						 "Open", GTK_RESPONSE_ACCEPT,
						 NULL);

    // Connect signals
    g_signal_connect(runButton, "clicked", G_CALLBACK(runButton_click), NULL);
    g_signal_connect(loadButton, "clicked", G_CALLBACK(loadButton_click), NULL);
    g_signal_connect(quitButton, "clicked", G_CALLBACK(mainWindow_quit), NULL);

    // Pack controls for the first tab
    mainVbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(mainVbox), runButton, true, true, 0);
    gtk_box_pack_start(GTK_BOX(mainVbox), loadButton, true, true, 0);
    gtk_box_pack_start(GTK_BOX(mainVbox), quitButton, true, true, 0);
    gtk_box_pack_start(GTK_BOX(mainVbox), statusbar, false, true, 0);

    // Add controls for the options pane
    optionsBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    
    delayExpander = gtk_expander_new_with_mnemonic("Execution Delay");
    fcolourExpander = gtk_expander_new_with_mnemonic("Forecolour");
    bcolourExpander = gtk_expander_new_with_mnemonic("Backcolour");
    
    delayFixed = gtk_fixed_new();
    fcolourFixed = gtk_fixed_new();
    bcolourFixed = gtk_fixed_new();
    
    delaySpinner = gtk_spin_button_new_with_range(1.0, 250.0, 1.0);
    // If executionSpeed != -1, set the value of the delay spinner to executionSpeed, otherwise 2
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(delaySpinner), executionSpeed != -1 ? executionSpeed : 2);

    if (!has_colour_init)
    {
	gdk_rgba_parse(&fcolour, "#FFFFFF");
	gdk_rgba_parse(&bcolour, "#000000");

	has_colour_init = true;
    }

    fcolourButton = gtk_color_button_new_with_rgba(&fcolour);
    bcolourButton = gtk_color_button_new_with_rgba(&bcolour);
    
    gtk_fixed_put(GTK_FIXED(delayFixed), delaySpinner, 15, 5);
    gtk_fixed_put(GTK_FIXED(fcolourFixed), fcolourButton, 15, 5);
    gtk_fixed_put(GTK_FIXED(bcolourFixed), bcolourButton, 15, 5);
    
    gtk_container_add(GTK_CONTAINER(delayExpander), delayFixed);
    gtk_container_add(GTK_CONTAINER(fcolourExpander), fcolourFixed);
    gtk_container_add(GTK_CONTAINER(bcolourExpander), bcolourFixed);
    gtk_box_pack_start(GTK_BOX(optionsBox), delayExpander, false, false, 5);
    gtk_box_pack_start(GTK_BOX(optionsBox), fcolourExpander, false, false, 5);
    gtk_box_pack_start(GTK_BOX(optionsBox), bcolourExpander, false, false, 5);

    notebook = gtk_notebook_new();
    notebookLabels[0] = gtk_label_new("Home");
    notebookLabels[1] = gtk_label_new("Options");
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), mainVbox, notebookLabels[0]);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), optionsBox, notebookLabels[1]);

    gtk_container_add(GTK_CONTAINER(mainWindow), notebook);
    gtk_widget_show_all(mainWindow);

    gtk_main();

    return filename;
}

void gui_quit()
{
    gtk_widget_hide(mainWindow);
    gtk_main_quit();
}

void gui_getcolours(uint32_t* fg, uint32_t* bg)
{
    gtk_color_button_get_rgba(GTK_COLOR_BUTTON(fcolourButton), &fcolour);
    *fg = ((int)(fcolour.red * 255) << 16) | ((int)(fcolour.green * 255) << 8) | (int)(fcolour.blue * 255);

    gtk_color_button_get_rgba(GTK_COLOR_BUTTON(bcolourButton), &bcolour);
    *bg = ((int)(bcolour.red * 255) << 16) | ((int)(bcolour.green * 255) << 8) | (int)(bcolour.blue * 255);
}

int gui_getExecSpeed()
{
    executionSpeed = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(delaySpinner));
    return executionSpeed;
}

// GUI callback functions
bool mainWindow_quit(GtkWidget* widget, gpointer data)
{
    gui_quit();
    *quitCondition = true;
    return true;
}

bool runButton_click(GtkWidget* widget, gpointer data)
{
    gui_quit();
    return true;
}

bool loadButton_click(GtkWidget* widget, gpointer data)
{
    if (gtk_dialog_run(GTK_DIALOG(openFileDialog)) == GTK_RESPONSE_ACCEPT)
    {
	GtkFileChooser* chooser = GTK_FILE_CHOOSER(openFileDialog);

	filename = gtk_file_chooser_get_filename(chooser);

	// Extract the filename
	int sepIndex = -1;
	for (int i = strlen(filename) - 1; i >= 0; i--)
	    if (filename[i] == DIR_SEPERATOR) { sepIndex = i; break; }

	if (sepIndex != -1)
	    strncpy(displayName, filename + sepIndex + 1, strlen(filename) - sepIndex);

	strcpy(statusbarText, "Loaded: ");
	strcat(statusbarText, sepIndex != -1 ? displayName : filename);
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), statusbarContext, statusbarText);
    }

    gtk_widget_hide(openFileDialog);
    return true;
}
