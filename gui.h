#ifndef GUI_H
#define GUI_H

#include <gtk/gtk.h>
#include "global.h"

char* gui_init(int* argc, char*** argv, bool* quitCond, char* fname);
void gui_resume();
void gui_quit();

void gui_getcolours(uint32_t* fg, uint32_t* bg);
int gui_getExecSpeed();

#endif
